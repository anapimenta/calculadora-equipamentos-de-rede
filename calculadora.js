window.onload = function () {
//Variáveis para as entradas
let imgBotao = document.querySelector("#voltar").addEventListener('click', function(){
  location.reload();
});
//funcao que mostra o resultado
 document.getElementById('calcular').addEventListener('click', function() {
   let numPtsTelecom = document.querySelector('#telecom').value;
   let numPtsSimples = document.querySelector('#simples').value;
   let numPtsCFTV = document.querySelector('#cftv').value;
   let numPtsVozIP = document.querySelector('#voz').value;
   let distanciaMediaMH = document.querySelector('#distanciaMedia').value;
   let catCabos = "5e";
   let tipoRack = document.querySelector('#rack').value;


   // Variáveis para as saídas
   let numTomadasFemeas = parseInt(numPtsTelecom * 2) + parseInt(numPtsSimples);
   let numPatchCords = parseInt(numPtsTelecom * 2) + parseInt(numPtsSimples);
   let numEspelhosDuplos = numPtsTelecom;
   let numEspelhosSimples = numPtsSimples;
   let qtdCaboMH = parseInt(numTomadasFemeas) * parseInt(distanciaMediaMH);
   let EtiquetasMH = parseInt(3 * numPtsTelecom) + parseInt(2 * numPtsSimples) + parseInt(2 * numPtsTelecom) + (2 * numPtsSimples);
   let numPatchPanels = Math.ceil(numTomadasFemeas / 24);
   let totalPatchCables = numTomadasFemeas;
   let patchCableDados = numTomadasFemeas - parseInt(numPtsVozIP) - parseInt(numPtsCFTV);
   let patchCableVoz = numPtsVozIP;
   let patchCableCFTV = numPtsCFTV;
   let numSwitches = numPatchPanels;
   let numOrgCabo = parseInt(numPatchPanels) + parseInt(numSwitches);
   let etiquetasPatchPanel = numPatchPanels * 24;
   let somaTamanhoEquip = numPatchPanels + numOrgCabo + numSwitches;
   let qtdRacks = null;
   let numFiltrosLinha = Math.ceil((numPatchPanels+numSwitches) / 8)
   let tamanhoRack = null;


  if(tipoRack == 'fechado'){
    tamanhoRack = (somaTamanhoEquip+6)* 1.5;
  }
  else{
    tamanhoRack = (somaTamanhoEquip)* 1,5;
  }

  tamanhoRack = tamanhoRackCalc(tamanhoRack);

  console.log(tamanhoRack);

  qtdRacks = Math.ceil(tamanhoRack/48);
  let tamanhoRackIndividual = Math.ceil(tamanhoRack/qtdRacks);
  tamanhoRackIndividual = tamanhoRackCalc(tamanhoRackIndividual);

  let abrcVelcro = 3 * qtdRacks;
  let numPorcaG = qtdRacks * ( 4 * somaTamanhoEquip);
  let abrcHellermann = 1 * qtdRacks;


   esconde();

   addTabelas("Tomadas Femea", numTomadasFemeas);
   addTabelas("Patch cord", numPatchCords);
   addTabelas("Espelho de furação dupla", numEspelhosDuplos);
   addTabelas("Espeljo de furação simples", numEspelhosSimples);
   addTabelas("Cabo para malha horizontal (metros)", qtdCaboMH );
   addTabelas("Etiquetas de malha horizontal", etiquetasPatchPanel);
   addTabelas("Patch panel", numPatchPanels);
   addTabelas("Patch cable(total)", totalPatchCables);
   addTabelas("Patch cable de dados(azul)", patchCableDados);
   addTabelas("Patch cable de Voz(amarelo)", patchCableVoz);
   addTabelas("Patch cable de CFTV(vermelho)", patchCableCFTV);
   addTabelas("Switch", numSwitches);
   addTabelas("Organizador de cabo", numOrgCabo);
   addTabelas("Etiqueta de Patch panel", etiquetasPatchPanel);
   addTabelas("Filtro de linha", numFiltrosLinha);

   addTabelas("Rack", qtdRacks+" rack(s) de "+tamanhoRackIndividual);


   addTabelas("Abraçadeira de velcro", abrcVelcro);
   addTabelas("porca - gaiola", numPorcaG);
   addTabelas("Abraçadeira Hellermann (pacote com 100 unidades)", abrcHellermann);



});

};

function esconde(){
  let containerEntrada = document.querySelector("#mainContainer");
  let containerResultado = document.querySelector("#containerResultado")
  let imgBotao = document.querySelector("#voltar");
  containerEntrada.classList.toggle("escondido");
  containerResultado.classList.toggle("escondido");
  imgBotao.classList.toggle("escondido");
}

function tamanhoRackCalc(tamanhoRack){
  let contador = 2;

  if (tamanhoRack <= contador) {
    tamanhoRack = contador;
  }

 for (var i = 0; i < 5; i++) {
   if (tamanhoRack > contador && tamanhoRack <= contador+2) {
     tamanhoRack = contador+2;
   }
   contador+=2;
 }
 for (var i = 0; i < 9; i++) {
   if (tamanhoRack > contador && tamanhoRack <= contador+4) {
     tamanhoRack = contador+4;
   }
   contador+=4;
 }
 return tamanhoRack;
}


function addTabelas(nome, valor){
  var tabelaResultado = document.querySelector("#resultado");
  var novotr = document.createElement('tr');
  var novotd =document.createElement('td');

  var node  = document.createTextNode(nome);
  novotd.appendChild(node);

  novotr.appendChild(novotd);

  node  = document.createTextNode(valor);
  novotd =document.createElement('td');
  novotd.appendChild(node);

  novotr.appendChild(novotd);

  tabelaResultado.appendChild(novotr);
}
